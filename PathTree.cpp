#include <iostream>
#include <vector>
using std::vector;

struct Point {
  int x;
  int y;
};

class Path {
  private:
  Point origin;
  Path *parent = NULL;
  Path *child = NULL;
  bool first,last;
  Path(int x, int y, Path *p) {
    origin = {x,y};
    parent = p;
    last = true;
    first = p == NULL;
  }

  public:
  Path(int x, int y) : Path(x,y,NULL){};
  Path *next() {
    return child;
  }
  Path *prev() { //previous
    return parent;
  }
  void add(int x, int y) {
    if (last) {
      child = new Path(x,y,this);
      last = false;
    }
    else {
      child->add(x,y);
    }
  }
  Point point() {
    return origin;
  }
  Path *reverse() { //reverses path and returns new first node
    Path *temp_res = NULL;
    if(!last) {
      temp_res = child->reverse();
    }
    Path *temp = parent;
    parent = child;
    child = temp;
    if(first) {
      first = false;
      last = true;
    }
    else if(last) {
      last = false;
      first = true;
      return this;
    }
    return temp_res; //placeholder
  }
  ~Path() {
    delete child;
  }
};

class PathTree {
    private:
	int x, y;
	vector<PathTree*> branches;
	bool start, end, fin;
	PathTree *parent = NULL;//cannot be deleted
	PathTree(int ax, int ay, PathTree* p) {
	    x = ax;
	    y = ay;
	    parent = p;
	    start = p == NULL;
	    end = true;
	    fin = false;
	}
    public:   
	PathTree(int x, int y)
	    : PathTree(x,y,NULL){}
	PathTree()
	    : PathTree(0,0,NULL){}
	Point point(){
	    return Point{x,y};
	}
  int getX() {
    return x;
  }
  int getY() {
    return y;
  }
	void add(int ax, int ay) {
	    branches.push_back(new PathTree(ax, ay, this));
	    end = false;
	}
	void remove(int i) {
	    delete branches.at(i);
	    branches.erase(branches.begin()+i);
	    branches.shrink_to_fit();
	    if (branches.empty()) end = true;
	}
	int children() {
	    return branches.size();
	}
  bool begin() {
    return start;
  }
	bool finish() {
	    return fin;
	}
	void setFinish() {
	    fin = !fin;
	}
  PathTree *prev() {
    return parent;
  }
	PathTree *branch(int i) {
	    return branches.at(i);
	}
	void for_each(void(*f)(PathTree*)) {//for the current point and each point below
    f(this);
    if(!end) {
      for(auto i : branches) {
        i->for_each(f);
      }
    }
	}
	void for_each_end(void(*f)(PathTree*)) {//only for endpoints
    if(end)
      f(this);
    else {
      for(auto i : branches) {
        i->for_each_end(f);
      }
    }
	}
  PathTree *getFinish() {
    PathTree *res = this;
    if(fin){
      
    }
    else {
      for(auto i : branches) {
        res = i->getFinish();
      }
    }
    return res;
  }
  Path* for_finish(Path*(*f)(PathTree*)) {//only for endpoints
    /*if(fin && end){
      std::cout << "fin found\n";
      return f(this);
    }
    else {
      for(auto i : branches) {
      std::cout << "floop\n";
      return i->for_finish(f);
      }
    }*/
    return f(this->getFinish());
    //std::cout << "hmmm";
    //return NULL; //shouldn't ever land here but just in case
	}
  Path *genPath() {
    Path *res = NULL;
    //std::cout << "test\n";
    res = this->for_finish([](PathTree *n) mutable -> Path* {
      //std::cout << "test\n";
      Path *path = new Path(n->getX(),n->getY());
      PathTree *iter = n->prev();
      int timeout = 0;
      while(timeout != -1) {
        //std::cout << "loop\n";
        ++timeout;
        if(iter != NULL) {
          path->add(iter->getX(),iter->getY());
          iter = iter->prev();
        }
        else {
          //std::cout << "null\n";
          timeout = -1;
        }
        if (timeout > 2000){
          //std::cout << "timeout\n";
          break;
        }
      }
      return path;
    });
    //std::cout << "test\n";
    if (res == NULL) std::cout << "oopsy\n";
    return res->reverse();
  }
	~PathTree() {
	  for(PathTree* x : branches) {
		  delete x;
	  }
    branches.clear();
    branches.shrink_to_fit();
	}

};

int main() {//testing
  Path *x = new Path(0,0);
  x->add(1,1);
  x->add(2,2);
  std::cout << x->point().x;
  std::cout << x->next()->point().x;
  std::cout << x->next()->next()->point().x << "\n";
  x = x->reverse();
  std::cout << x->point().x;
  std::cout << x->next()->point().x;
  std::cout << x->next()->next()->point().x << "\n";

  PathTree *y = new PathTree();
  y->add(1,1);
  y->branch(0)->add(2,2);
  y->branch(0)->add(3,3);
  y->branch(0)->branch(1)->setFinish();
  std::cout << y->branch(0)->branch(1)->finish() << "\n";
  std::cout << y->branch(0)->branch(0)->finish() << "\n";
  y->for_each_end([](PathTree *n){std::cout << (n->finish()?"true":"false") << ",";});
  std::cout << "\n";
  std::cout << "fin:" << y->getFinish()->getX() << "\n";
  Path *test = y->genPath();
  std::cout << test->point().x;
  std::cout << test->next()->point().x;
  std::cout << test->next()->next()->point().x << "\n";
  return 0;
}