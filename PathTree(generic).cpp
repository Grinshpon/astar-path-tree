#include <iostream>
#include <vector>
using std::vector;

struct Point {
  int x;
  int y;
};

template<class T>
class PathTree {
    private:
	T data;
	vector<PathTree*> branches;
	bool start, end, fin;
	PathTree *parent = NULL;//cannot be deleted
	PathTree(T d, PathTree* p) {
	    data = d;
	    parent = p;
	    start = p == NULL;
	    end = true;
	    fin = false;
	}
    public:   
	PathTree(T d)
	    : PathTree(d,NULL){}
	T get(){
	    return data;
	}
	void add(T d) {
	    branches.push_back(new PathTree(d, this));
	    end = false;
	}
	void remove(int i) {
	    delete branches.at(i);
	    branches.erase(branches.begin()+i);
	    branches.shrink_to_fit();
	    if (branches.empty()) end = true;
	}
	int children() {
	    return branches.size();
	}
	bool finish() {
	    return fin;
	}
	void setFinish() {
	    fin = !fin;
	}
	PathTree *branch(int i) {
	    return branches.at(i);
	}
	void for_each(void(*f)(PathTree*)) {//for the current point and each point below
	    f(this);
      if(!end) {
        for(auto i : branches) {
          i->for_each(f);
        }
      }
	}
	void for_each_end(void(*f)(PathTree*)) {//only for endpoints
	    if(end)
        f(this);
      else {
        for(auto i : branches) {
          i->for_each_end(f);
        }
      }
	}
	~PathTree() {
	    for(PathTree* x : branches) {
		delete x;
	    }
	    branches.clear();
	    branches.shrink_to_fit();
	}

};

int main() {//testing
  /*PathTree *x = new PathTree();//0,0
  x->add(5,2);
  x->add(1,3);
  x->branch(1)->add(9,9);
  std::cout << x->branch(1)->branch(0)->point().x << "\n";
  x->for_each([](PathTree *n){std::cout << n->point().y;});//0239
  std::cout << "\n";
  x->for_each_end([](PathTree *n){std::cout << n->point().y;});//29
  */
  PathTree<Point> *x = new PathTree<Point>({0,1});
  x->add({2,2});
  x->branch(0)->add({3,3});
  x->add({4,4});
  std::cout << x->get().y << "\n\n";
  x->for_each([](PathTree<Point> *n){std::cout << n->get().x << "\n";});
  std::cout << "\n";
  x->for_each_end([](PathTree<Point> *n){std::cout << n->get().y << "\n";});
  return 0;
}